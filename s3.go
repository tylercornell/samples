package main

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/garyburd/redigo/redis"
	"strconv"
	"strings"
	"time"
)

var AWS_REGION = "us-east-1"
var aws_session, _ = session.NewSession(&aws.Config{Region: aws.String(AWS_REGION)})

// var aws_s3_session = s3.New(aws_session)
var s3_uploader = s3manager.NewUploader(aws_session)

func main() {

	now := time.Now().UTC()

	//Preparing date YY MM DD HH
	year := leftPad2Len(strconv.Itoa(now.Year()), "0", 4)
	//fmt.Println(year)
	month := leftPad2Len(strconv.Itoa(int(now.Month())), "0", 2)
	//fmt.Println(month)
	day := leftPad2Len(strconv.Itoa(now.Day()), "0", 2)
	//fmt.Println(day)
	hour := leftPad2Len(strconv.Itoa(now.Hour()), "0", 2)
	//fmt.Println(hour)

	//Bucket/S3 Directory Naming
	bucket_name := "motive-bidder"
	root_directory := "logs"
	log_type := "win-logs"
	date_paths := year + "/" + month + "/" + day + "/" + hour
	file_name := "win-log-" + strconv.FormatInt(now.UnixNano(), 10)
	path := root_directory + "/" + log_type + "/" + date_paths + "/" + file_name

	fmt.Println("Uploading latest augmentor requests from Redis to S3... \n")

	//Get Records up until now
	result := fetchFromRedis("winlog", now, true)

	fmt.Println(path)

	//Upload to S3
	uploadToS3(result, bucket_name, path)
}

func fetchFromRedis(hashname string, time_of_pull time.Time, remove_after_fetch bool) interface{} {
	var fetched_records interface{}

	//open connection
	redisConn, err := redis.Dial("tcp", "127.0.0.1:6379")

	//create
	now := time_of_pull
	now_unix_nano := now.UnixNano()
	//convert to milliseconds
	now_unix_millis := now_unix_nano / 1000000
	now_unix_millis_str := strconv.FormatInt(now_unix_millis, 10)

	fmt.Println(now_unix_millis_str)

	if err != nil {
		fmt.Println(err.Error())
		return nil
	} else {
		defer redisConn.Close()
		//Fetch the sorted set up to the current millisecond timestamp
		result, err3 := redisConn.Do("ZRANGEBYSCORE", hashname, "0", now_unix_millis_str)
		if err3 != nil {

			fmt.Println(err3.Error())
			return nil
		} else {
			fetched_records = result
		}
	}
	if remove_after_fetch {
		_, err3 := redisConn.Do("ZREMRANGEBYSCORE", hashname, "0", now_unix_millis_str)
		if err3 != nil {
			fmt.Println(err3.Error())
		}
	}

	return fetched_records
}

func leftPad2Len(s string, padStr string, overallLen int) string {
	//This adds a padding to a string and returns the proper range  EX: leftPad2Len("3", "0" , 2) returns "03"
	var padCountInt int
	padCountInt = 1 + ((overallLen - len(padStr)) / len(padStr))
	var retStr = strings.Repeat(padStr, padCountInt) + s
	return retStr[(len(retStr) - overallLen):]
}

func uploadToS3(redis_records interface{}, bucket_name string, filename string) {
	this_string_buffer := new(bytes.Buffer)
	//Write redis results to buf
	for _, k := range redis_records.([]interface{}) {
		this_byte_array := k.([]byte)
		this_string_buffer.WriteString(string(this_byte_array) + ",\n")
	}

	if this_string_buffer.Len() > 0 {
		//gzip it up and send on to S3
		var gz_buffer bytes.Buffer
		gz_writer := gzip.NewWriter(&gz_buffer)
		_, err := gz_writer.Write(this_string_buffer.Bytes())
		if err == nil {
			if err := gz_writer.Flush(); err == nil {
				if err := gz_writer.Close(); err == nil {
					result, err2 := s3_uploader.Upload(&s3manager.UploadInput{
						Body:   bytes.NewReader(gz_buffer.Bytes()),
						Bucket: aws.String(bucket_name),
						Key:    aws.String(filename + ".gz"),
					})
					if err2 != nil {
						// fmt.Println(err2.Error())
					} else {
						fmt.Println("Successfully uploaded to", result.Location)
					}
				}
			}
		}
	}
}
