package main
import (
	//"encoding/json"
	"flag"
	"fmt"
	"github.com/garyburd/redigo/redis"
	"io/ioutil"
	//"log"
	"net/http"
	// "os"
	"strconv"
	"time"
)

func newPool() *redis.Pool {
	return &redis.Pool{
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", "127.0.0.1:6379")
			if err != nil {
			}
			return c, err
		},
		// IdleTimeout: 60,
		MaxIdle:   1024,
		MaxActive: 300000,
		Wait:      true,
	}

}

var pool = newPool()

type redis_job struct {
	//json
	adRequest string
}

type redis_worker struct {
	id int
}

func (w redis_worker) process_redis(j redis_job) {
	storeInRedisByTimestamp("winlog", j.adRequest)
}

func main() {
	var (
		max_queue_size    = flag.Int("max_queue_size", 100, "The size of job queue")
		max_redis_workers = flag.Int("max_redis_workers", 5, "The number of redis workers to start")
		port              = flag.String("port", "8080", "The server port")
	)
	flag.Parse()

	// create job channel
	redis_job_ch := make(chan redis_job, *max_queue_size)

	// create redis workers
	for i := 0; i < *max_redis_workers; i++ {
		w := redis_worker{i}
		go func(w redis_worker) {
			for j := range redis_job_ch {
				w.process_redis(j)
			}
		}(w)
	}

	fmt.Println("Server Starting..")

	//Test via http - should return pong. http://127.0.0.1:8080/ping
	http.HandleFunc("/ping", func(rw http.ResponseWriter, req *http.Request) {
		rw.Write([]byte("pong"))
	})

	//The endpoint Once running, test with: curl -H "Content-Type: application/json" -X POST -d '{"req_id":1,"app_bundle":"com.test.1"}' http://localhost:8080/exec
	http.HandleFunc("/exec", func(rw http.ResponseWriter, req *http.Request) {
		// handleRequest(redis_job_ch, rw, req)
		handleRequest(redis_job_ch, rw, req)
	})

	// log.Fatal(http.ListenAndServe(":"+*port, nil))
	http.ListenAndServe(":"+*port, nil)
}

func handleRequest(redis_job_ch chan redis_job, rw http.ResponseWriter, req *http.Request) {
	//Read binary from the POST
	buf, err := ioutil.ReadAll(req.Body)

	if err != nil {
		http.Error(rw, "HTTP Read Error", 204)
		return
	}

	//will just output binary if isn't casted to string first
	adRequest := string(buf)

	fmt.Println("POST:")
	fmt.Println(adRequest)
	fmt.Println("\n")
	/*
		Note: Define Structs to access individual elements
		http://stackoverflow.com/questions/15672556/handling-json-post-request-in-go
	*/

	/* If no request body
	if len(buf) < 1 {
		error_number := 501
		http.Error(rw, "No Request Body", error_number)
		return
	}*/

	//Set header params
	//rw.Header().Set("Content-Type", "application/x-protobuf")
	
	//Write header response
	//rw.WriteHeader(200)

	// Create Redis Job and push the work onto the job channel.
	go func() {
		redis_job := redis_job{adRequest}
		redis_job_ch <- redis_job
	}()
}

func storeInRedisByTimestamp(hashname string, adRequest string) {
	fmt.Println("Storing in Redis..")
	fmt.Println(adRequest)
	now := time.Now()
	now_unix_nano := now.UnixNano()
	now_unix_millis := now_unix_nano / 1000000
	now_unix_nano_str := strconv.FormatInt(now_unix_nano, 10)
	now_unix_millis_str := strconv.FormatInt(now_unix_millis, 10)
	fmt.Println(now_unix_millis_str);

	json := "[" + now_unix_nano_str + ", " + adRequest + "]"

	redisConn := pool.Get()
	defer redisConn.Close()
	//Sorted range - See the input in redis-cli command: zrange winlog 0 -1
	_, err := redisConn.Do("ZADD", hashname, now_unix_millis_str, json)
	if err != nil {
	}
	//redisConn.Close()
}
